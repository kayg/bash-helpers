# Bash has some very memeworthy behaviour by default. The other day somebody on
# Reddit was crying because he accidentally deleted all files in his root
# directory because:
#
# 1. Their script did not terminate when a command failed.
# 2. The failing command's purpose was to assign a base path to a variable.
#
# And this happens every other day to some shell beginner out there. Hence, this
# script tries to restore sanity to shell scripts.
#
# errexit / -e: terminates the script immediately if a command returns a
# non-zero exit code. It can be temporary bypassed by appending a =|| true=
# which makes the complete command exit with zero.
#
# nounset / -u: ensures all variables have been assigned a value before they are
# referred to. Upon encountering an undefined variable, terminates the script.
# VAR= is a valid assignment.
#
# pipefail: terminates the script immediately if part of the pipe chain exits
# with a non-zero code.
#
# xtrace / -x: enables debug mode. Commands are shown as they're executed.
# 
# References: https://kvz.io/blog/2013/11/21/bash-best-practices/
# https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/

set -o errexit
set -o pipefail
set -o nounset

# https://stackoverflow.com/questions/17852801/how-to-let-bash-subshell-to-inherit-parents-set-options
#
# The following makes it so that the above set shell options propagate through
# child shells and scripts.
export SHELLOPTS

# Runs a recursive removal on the files and directories provided as arguments.
clean() {
    printf "Cleaning up all the cruft."

    if [[ -n "${@}" ]]; then
        run rm --force \
            --recursive \
            "${@}"
    else
        printf "Invalid number of arguments.\n"
    fi
}

# Performs a git clone on the URL provided as argument at the directory provided
# as second argument.
fetch_src() {
    local url="${1}"
    local dest="${2}"

    if [[ -d "${dest}" ]]; then
        clean "${dest}"
    fi

    printf "Fetching ${url}."
    run git clone \
        --recursive \
        --quiet \
        "${url}" "${dest}"
}

# A wrapper for prettifying output through:
# - disabling error and output streams
# - replacing those streams by a cross and a tick
#
# More of a hack at this point as some commands refuse to work.
run() {
    if eval "${@}" 2>/dev/null 1>&2; then
        printf " \u2714\n"
    else
        printf " \u274c\n"
        exit 1
    fi
}

# Replacement for run() when commands simply refuse to work inside an eval. I
# continue to use bash, but why?
status() {
    if [[ "${?}" -eq 0 ]]; then
          printf " \u2714\n"
    else
          printf " \u274c\n"
          exit 1
    fi
}
